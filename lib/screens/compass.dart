import 'package:flutter/material.dart';
import 'package:my_confinement/widgets/compass/compassWidget.dart';
import 'package:my_confinement/widgets/sidebar/navbloc.dart';

class Compass extends StatelessWidget with NavigationStates{
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CompassWidget(),
    );
  }
}