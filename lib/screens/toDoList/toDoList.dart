import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_confinement/models/toDo.dart';
import 'package:my_confinement/screens/toDoList/formTDL.dart';
import 'package:my_confinement/services/databaseHelper.dart';
import 'package:my_confinement/shared/french.dart';
import 'package:my_confinement/widgets/notification/notificationManager.dart';
import 'package:my_confinement/widgets/sidebar/navbloc.dart';
import 'package:my_confinement/widgets/todoList/swipeWidget.dart';
import 'package:sqflite/sqflite.dart';

class ToDoList extends StatefulWidget with NavigationStates {
  @override
  _ToDoListState createState() => _ToDoListState();
}

class _ToDoListState extends State<ToDoList> {
  DatabaseHelper databaseHelper = DatabaseHelper();
  List<Todo> todoList;
  int count = 0;

  bool refresh = false;

  @override
  Widget build(BuildContext context) {
    if (todoList == null) {
      todoList = List<Todo>();
      updateListView();
    }

    void _showAddItem() async {
      await showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
              child: FormAddItem(Todo('', '', ''),NotificationManager()),
            );
          }).whenComplete(() {
        setState(() {
          updateListView();
        });
      });
    }

    return Padding(
        padding: EdgeInsets.only(left: 20.0),
        child: Scaffold(
          body: Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(left: 30.0),
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.24,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        SizedBox(
                          height: 68,
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              formatDate(DateTime.now(), [DD, ', '],
                                      locale: FrenchLocale())
                                  .toString(),
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 26,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                                formatDate(DateTime.now(), [dd],
                                        locale: FrenchLocale())
                                    .toString(),
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 26,
                                    fontWeight: FontWeight.w400)),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                                formatDate(DateTime.now(), [MM, ' ', yyyy],
                                        locale: FrenchLocale())
                                    .toString(),
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black26)),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 10),
                          child: Text(
                              count == 1 ? "Une seule tâche" : "$count Tâches",
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black54)),
                        ),
                      ],
                    ),
                  )),
              Container(
                height: MediaQuery.of(context).size.height * 0.68,
                child: getTodoListView(),
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.black,
            onPressed: () {
              _showAddItem();
            },
            tooltip: 'Add Todo',
            child: Icon(Icons.add),
          ),
        ));
  }

  ListView getTodoListView() {
    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context, int position) {
        return new OnSlide(
            items: <ActionItems>[/*
              new ActionItems(
                  icon: new IconButton(
                    icon: new Icon(Icons.mode_edit),
                    onPressed: () {},
                    color: Colors.blue,
                  ),
                  onPress: () {
                  },
                  backgroudColor: Colors.white),*/
              new ActionItems(
                  icon: new IconButton(
                    icon: new Icon(Icons.delete),
                    onPressed: () {
                    },
                    color: Colors.red,
                  ),
                  onPress: () {
                    _delete(context, todoList[position]);
                  },
                  backgroudColor: Colors.white),
            ],
            child: Container(
              padding: const EdgeInsets.only(top:10.0),
              width: 200.0,
              height: 90.0,  
                child: new Card(
                color: Colors.white,
                elevation: 2.0,
                child: ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Icon(
                      getIconOfCategory(this.todoList[position].categorie),
                      color: Colors.black54,
                    ),
                  ),
                  title: Text(this.todoList[position].title,
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  subtitle: Text(this.todoList[position].categorie),
                  /*trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      GestureDetector(
                        child: Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        onTap: () {
                          _delete(context, todoList[position]);
                        },
                      ),
                    ],
                  ),
                  */
                  onTap: () {
                    debugPrint("ListTile Tapped");
                  },
                ),
              ),
            )
          );
      },
    );
  }

  getIconOfCategory(String categorie) {
    switch (categorie) {
      case 'Sport':
        return Icons.directions_bike;
      case 'Santé':
        return Icons.healing;
      case 'Alimentation':
        return Icons.local_dining;
      case 'Animaux':
        return Icons.pets;
      case 'Domestique':
        return Icons.home;
    }
  }

  void _delete(BuildContext context, Todo todo) async {
    int result = await databaseHelper.deleteTodo(todo.id);
    if (result != 0) {
      _showSnackBar(context, 'Todo Deleted Successfully');
      updateListView();
    }
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void updateListView() {
    final Future<Database> dbFuture = databaseHelper.initializeDatabase();
    dbFuture.then((database) {
      Future<List<Todo>> todoListFuture = databaseHelper.getTodoList();
      todoListFuture.then((todoList) {
        setState(() {
          this.todoList = todoList;
          this.count = todoList.length;
        });
      });
    });
  }
}
