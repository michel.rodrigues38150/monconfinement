import 'package:flutter/material.dart';
import 'package:my_confinement/widgets/map/googleMap.dart';
import 'package:my_confinement/widgets/sidebar/navbloc.dart';

class GoogleMap extends StatelessWidget with NavigationStates {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GMap(),
    );
  }
}