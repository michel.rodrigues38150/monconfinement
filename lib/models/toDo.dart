class Todo {

  //Attributs
  int _id;
  String _title;
  String _categorie;
  String _deadline;

  //Constructeur 
  Todo(this._title, this._categorie, this._deadline);

  //Constructeur avec id
  Todo.withId(this._id, this._title, this._categorie, this._deadline);

  //getters
  int get id => _id;

  String get title => _title;

  String get categorie => _categorie;

  String get deadline => _deadline;

  //setters
  set title(String newTitle){
    this._title = newTitle;
  }

  set categorie(String newcategorie) {
			this._categorie = newcategorie;
	}

	set deadline(String newDeadline) {
		this._deadline = newDeadline;
	}

  // Convert a Note object into a Map object
	Map<String, dynamic> toMap() {

		var map = Map<String, dynamic>();
		if (id != null) {
			map['id'] = _id;
		}
		map['title'] = _title;
		map['categorie'] = _categorie;
		map['deadline'] = _deadline;

		return map;
	}

	// Extract a Note object from a Map object
	Todo.fromMapObject(Map<String, dynamic> map) {
		this._id = map['id'];
		this._title = map['title'];
		this._categorie = map['categorie'];
		this._deadline = map['deadline'];
	}
}