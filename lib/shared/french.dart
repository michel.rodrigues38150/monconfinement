import 'package:date_format/date_format.dart';

class FrenchLocale implements Locale {
  const FrenchLocale();

  final List<String> monthsShort = const [
    'Jan',
    'Fev',
    'Mar',
    'Avr',
    'Mai',
    'Jun',
    'Jui',
    'Aout',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];

  final List<String> monthsLong = const [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre'
  ];

  final List<String> daysShort = const [
    'Lun',
    'Mar',
    'Mer',
    'Jeu',
    'Ven',
    'Sam',
    'Dim'
  ];

  final List<String> daysLong = const [
    'Lundi',
    'Mardi',
    'Mercredi',
    'Jeudi',
    'Vendredi',
    'Samedi',
    'Dimanche'
  ];
}