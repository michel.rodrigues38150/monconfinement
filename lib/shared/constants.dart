import 'package:flutter/material.dart';

const Duration animationDuration = Duration(milliseconds: 400);

const logo = "assets/img/logo.png";

const textBoxTemplate = InputDecoration(
  fillColor: Colors.white,
  filled: true,
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.black, width: 2.0)
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.black54, width: 2.0)
  )
);