import 'dart:math';

import 'package:flutter/material.dart';

class CompassPainter extends CustomPainter {

    CompassPainter({ @required this.angle }) : super();

    final double angle;
    double get rotation => -2 * pi * (angle / 360);

    Paint get _brush => new Paint()
        ..style = PaintingStyle.stroke
        ..strokeWidth = 2.0;

    @override
    void paint(Canvas canvas, Size size) {

        Paint circle = _brush
            ..color = Colors.black.withOpacity(0.6);

        Paint needle = _brush
            ..color = Colors.red[400];
        
        double radius = min(size.width / 2.2, size.height / 2.2);
        Offset center = Offset(size.width / 2, size.height / 2);
        Offset start = Offset.lerp(Offset(center.dx, radius), center, .4);
        Offset end = Offset.lerp(Offset(center.dx, radius), center, 0.1);
        
        canvas.translate(center.dx, center.dy);
        canvas.rotate(rotation);
        canvas.translate(-center.dx, -center.dy);
        canvas.drawLine(start, end, needle);
        canvas.drawCircle(center, radius, circle);
    }

    @override
    bool shouldRepaint(CustomPainter oldDelegate) => true;
}